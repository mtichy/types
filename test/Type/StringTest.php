<?php

namespace MTi\UnitTest;

use MTi\Type\UnicodeString;
use PHPUnit\Framework\TestCase;


class StringTest
    extends TestCase
{
    private $testStringLower = 'žluťoučký kůň pošel';
    private $testStringUpper = 'ŽLUŤOUČKÝ KŮŇ POŠEL';
    private $testStringFirstUpper = 'Žluťoučký kůň pošel';
    private $testStringCapitals = 'Žluťoučký Kůň Pošel';

    public function testUnicodeIsSet()
    {
        $this->assertEquals('UTF-8', mb_internal_encoding());
    }

    public function testToLower()
    {
        $this->assertEquals(
            $this->testStringLower
          , strval((new UnicodeString($this->testStringUpper))->lower())
        );
    }

    public function testToUpper()
    {
        self::assertEquals(
            $this->testStringUpper
          , strval((new UnicodeString($this->testStringLower))->upper())
        );
        self::assertEquals(
            $this->testStringFirstUpper
          , strval((new UnicodeString($this->testStringLower))->firstUpper())
        );
        self::assertEquals(
            $this->testStringCapitals
          , strval((new UnicodeString($this->testStringLower))->capitalize())
        );
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testStartsWith()
    {
        $s = new UnicodeString($this->testStringLower);
        self::assertTrue($s->startsWith('ž'));
        self::assertFalse($s->startsWith('Ž'));
        self::assertFalse($s->startsWith('a'));
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testEndsWith()
    {
        $s = new UnicodeString($this->testStringUpper);
        self::assertTrue($s->endsWith('L'));
        self::assertFalse($s->endsWith('l'));
        self::assertFalse($s->endsWith('Z'));
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testContains()
    {
        $s = new UnicodeString($this->testStringLower);
        self::assertTrue($s->contains('kůň'));
        self::assertTrue($s->contains(' '));
        self::assertFalse($s->contains('x'));
    }

    public function testSubstring()
    {
        $s = new UnicodeString($this->testStringLower);
        self::assertEquals('kůň', strval($s->substring(10, 3)));
        self::assertEquals('pošel', strval($s->substring(-5)));
    }

    public function testCamelCase()
    {
        $sHyphen = new UnicodeString('abcd-efgh-ijk');
        $sSnake = new UnicodeString('abcd_efgh_ijk');
        $expected = 'abcdEfghIjk';
        self::assertEquals($expected, strval($sHyphen->toCamelCase()));
        self::assertEquals($expected, strval($sSnake->toCamelCase()));
    }

    /**
     * @throws \MTi\RegexpException
     */
    public function testSnakeCase()
    {
        self::assertEquals('abcd_efgh_ijk', strval((new UnicodeString('abcdEfghIjk'))->toSnakeCase()));
    }
}
