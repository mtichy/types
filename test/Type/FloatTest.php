<?php

namespace MTi\UnitTest;

use MTi\Type\ExtendedFloat;
use PHPUnit\Framework\TestCase;


class FloatTest
    extends TestCase
{
    public function initializationPassProvider()
    {
        return [
            'zero' => ['0', 0, 0],
            'integer' => ['2', 1, 2.0],
            'positive' => ['12.12', 2, 12.12],
            'negative' => ['-12.21', 2, -12.21],
            'crop' => ['-12.21231', 2, -12.21],
        ];
    }

    public function initializationFailProvider()
    {
        return [
            'letter' => ['12S2'],
            'comma' => ['22,1999'],
            'start_dot' => ['.201'],
            'end_dot' => ['201.'],
        ];
    }

    /**
     * @dataProvider initializationPassProvider
     *
     * @param $floatstring
     * @param $decimals
     * @param $expected
     * @throws \MTi\InvalidNumberException
     */
    public function testFormatsPass(string $floatstring, int $decimals, float $expected)
    {
        self::assertEquals(
            $expected
          , (new ExtendedFloat($floatstring, $decimals))->float()
        );
    }

    /**
     * @dataProvider initializationFailProvider
     * @expectedException \MTi\InvalidNumberException
     *
     * @param $floatstring
     */
    public function testFormatsFail(string $floatstring)
    {
        new ExtendedFloat($floatstring);
    }

    /**
     * @expectedException \MTi\InvalidNumberException
     */
    public function testInitInvalidNull()
    {
        new ExtendedFloat(null);
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     * @throws \MTi\InvalidNumberException
     */
    public function testEquals()
    {
        $float1 = new ExtendedFloat('234.12',2);
        $float2 = new ExtendedFloat('234.122',2);
        $float3 = new ExtendedFloat('234.123',3);
        self::assertTrue($float1->equals($float2));
        self::assertFalse($float2->equals($float3));
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testAdd()
    {
        $float1 = new ExtendedFloat('1.12',2);
        $float2 = new ExtendedFloat('2.122',2);
        self::assertEquals(3.24,$float1->add($float2)->float());
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testSub()
    {
        $float1 = new ExtendedFloat('1.12',2);
        $float2 = new ExtendedFloat('2.122',2);
        self::assertEquals(-1,$float1->sub($float2)->float());
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testMul()
    {
        $float1 = new ExtendedFloat('1.12',2);
        $float2 = new ExtendedFloat('2',2);
        self::assertEquals(2.24,$float1->mul($float2)->float());
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testDiv()
    {
        $float1 = new ExtendedFloat('3.33',2);
        $float2 = new ExtendedFloat('3',2);
        self::assertEquals(1.11, $float1->div($float2)->float());
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     * @throws \MTi\InvalidNumberException
     */
    public function testComparation()
    {
        $float1 = new ExtendedFloat('3',2);
        $float2 = new ExtendedFloat('3.33',2);
        $float3 = new ExtendedFloat('3.330',3);

        self::assertTrue($float1->isLessThan($float2));
        self::assertFalse($float2->isLessThan($float1));

        self::assertTrue($float2->isAtLeast($float3));
        self::assertTrue($float2->isAtMost($float3));
        self::assertTrue($float2->isAtLeast($float1));
        self::assertFalse($float2->isAtMost($float1));
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testFloor()
    {
        $float = new ExtendedFloat('3.99',2);
        self::assertEquals(3.99, $float->float());
        self::assertEquals(3.0, $float->floor()->float());
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testFloorPrecision()
    {
        $float = new ExtendedFloat('3.3399',4);
        self::assertEquals(3.3399, $float->float());
        self::assertEquals(3.33, $float->floor(2)->float());
        $float = new ExtendedFloat('6.66666',4);
        self::assertEquals(6.6667, $float->float());
        self::assertEquals(6.66, $float->floor(2)->float());
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testCeil()
    {
        $float = new ExtendedFloat('3.3333',2);
        self::assertEquals(3.33, $float->float());
        self::assertEquals(4, $float->ceil()->float());
    }

    /**
     * @throws \MTi\InvalidNumberException
     */
    public function testCeilToPrecision()
    {
        $float = new ExtendedFloat('3.3333',4);
        self::assertEquals(3.3333, $float->float());
        self::assertEquals(3.34, $float->ceil(2)->float());
    }
}
