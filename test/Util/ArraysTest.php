<?php

namespace UnitTest;

use MTi\Util\Arrays;
use PHPUnit\Framework\TestCase;


class ArraysTest
    extends TestCase
{
    /**
     * @throws \PHPUnit\Framework\Exception
     */
    public function testMergeTree()
    {
        $a1 = [
            'a' => ['x'=>11, 'y'=>12],
            'b' => ['r'=>13, 't'=>14],
        ];
        $a2 = [
            'b' => ['q'=>16, 'w'=>15],
            'c' => ['n'=>123, 'gfd'=>434],
        ];
        $res = Arrays::mergeTree($a1, $a2);
        self::assertCount(3, $res);

        self::assertArrayHasKey('a', $res);
        self::assertArrayHasKey('b', $res);
        self::assertArrayHasKey('c', $res);

        self::assertArrayHasKey('x', $res['a']);
        self::assertArrayHasKey('y', $res['a']);
        self::assertArrayHasKey('r', $res['b']);
        self::assertArrayHasKey('t', $res['b']);
        self::assertArrayHasKey('q', $res['b']);
        self::assertArrayHasKey('w', $res['b']);
        self::assertArrayHasKey('n', $res['c']);
        self::assertArrayHasKey('gfd', $res['c']);
    }

    /**
     * @throws \PHPUnit\Framework\Exception
     */
    public function testMakePathPassEmtpy()
    {
        $a = [];
        Arrays::makePath($a, ['a', 'b', 'c']);
        self::assertArrayHasKey('a', $a);
        self::assertInternalType('array', $a['a']);
        self::assertArrayHasKey('b', $a['a']);
        self::assertInternalType('array', $a['a']['b']);
        self::assertArrayHasKey('c', $a['a']['b']);
        self::assertInternalType('array', $a['a']['b']['c']);
    }

    /**
     * @throws \PHPUnit\Framework\Exception
     */
    public function testMakePathPassFilled()
    {
        $a = [
            'a' => [
                'b' => [
                    'c' => [
                        'xxx' => 123,
                    ]
                ],
            ],
        ];
        Arrays::makePath($a, ['a', 'b', 'c']);
        self::assertArrayHasKey('a', $a);
        self::assertInternalType('array', $a['a']);
        self::assertArrayHasKey('b', $a['a']);
        self::assertInternalType('array', $a['a']['b']);
        self::assertArrayHasKey('c', $a['a']['b']);
        self::assertInternalType('array', $a['a']['b']['c']);
        self::assertArrayHasKey('xxx', $a['a']['b']['c']);
        self::assertEquals(123, $a['a']['b']['c']['xxx']);
    }
}
