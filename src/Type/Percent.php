<?php

namespace MTi\Type;


class Percent
    extends ExtendedFloat
{
    private function formattedNumber(int $prec)
    {
        $nr = clone $this;
        return number_format(
            $nr
                ->mul(pow(10, $prec))
                ->ceil()
                ->div(pow(10, $prec))
                ->float()
          , $prec
          , ','
          , '.'
        );
    }

    private function withSuffix(int $prec): string
    {
        return sprintf('%s%%', $this->formattedNumber($prec));
    }

    /**
     * @return string
     */
    public function twoDecimalsPercentWithSuffix(): string
    {
        return $this->withSuffix(2);
    }

    /**
     * @return string
     */
    public function threeDecimalsPercentWithSuffix(): string
    {
        return $this->withSuffix(3);
    }
}
