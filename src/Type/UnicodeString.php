<?php

namespace MTi\Type;

use MTi\RegexpException;
use Nette\InvalidStateException;
use Nette\Utils\Strings;


class UnicodeString
{
    const TRIM_LEFT = -1;
    const TRIM_BOTH = 0;
    const TRIM_RIGHT = 1;

    /**
     * Returns a specific character.
     *
     * @param string $code
     * @param string $encoding
     * @return string
     */
    public static function chr(string $code, string $encoding = 'UTF-8'): string
    {
        return iconv('UTF-32BE', $encoding . '//IGNORE', pack('N', $code));
    }

    /**
     * @param int $length
     * @param string $charlist
     * @return self
     */
    public static function random(int $length = 10, string $charlist = '0-9a-z'): self
    {
        $charlist = str_shuffle(
            preg_replace_callback(
                '#.-.#'
              , function($m)
                {
                    return implode('', range($m[0][0], $m[0][2]));
                }
              , $charlist
            )
        );
        $chLen = strlen($charlist);
        $s = '';
        $rand = $rand2 = 0;
        for ($i = 0; $i < $length; $i++) {
            if ($i % 5 === 0) {
                $rand = lcg_value();
                $rand2 = microtime(TRUE);
            }
            $rand *= $chLen;
            $s .= $charlist[($rand + $rand2) % $chLen];
            $rand -= (int) $rand;
        }
        return new self($s);
    }

    ###############################

    /**
     * UnicodeString constructor.
     *
     * @param mixed $s
     */
    public function __construct($s)
    {
        $this->_s = strval($s);
    }
    private $_s;

    /**
     * @param mixed $val
     * @return self
     */
    protected function getValue($val): self
    {
        if ($val instanceof self) {
            return $val;
        }
        return new self(strval($val));
    }

    public function __toString()
    {
        return strval($this->_s);
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return '' === strval($this);
    }

    /**
     * @param string|UnicodeString $needle
     * @return bool
     */
    public function startsWith( $needle): bool
    {
        return Strings::startsWith($this->_s, strval($needle));
    }

    /**
     * @param string|UnicodeString $needle
     * @return bool
     */
    public function endsWith($needle): bool
    {
        return Strings::endsWith($this->_s, strval($needle));
    }

    /**
     * @param string|UnicodeString $needle
     * @return bool
     */
    public function contains($needle): bool
    {
        return Strings::contains($this->_s, strval($needle));
    }

    /**
     * @param int $start
     * @param int $length
     * @return self
     */
    public function substring(int $start, ?int $length = NULL): self
    {
        return new self(Strings::substring($this->_s, $start, $length));
    }

    /**
     * Removes special controls characters and normalizes line endings and spaces.
     *
     * @return self
     */
    public function normalize(): self
    {
        $this->_s = Strings::normalize($this->_s);
        return $this;
    }

    /**
     * Converts to ASCII.
     *
     * @return string ASCII
     */
    public function getAsciiValue(): string
    {
        return Strings::toAscii($this->_s);
    }

    /**
     * Converts to web safe characters [a-z0-9-] text.
     *
     * @param string $charlist allowed characters
     * @param bool $lower
     * @return string
     */
    public function getWebalizedValue(string $charlist = NULL, bool $lower = TRUE): string
    {
        return Strings::webalize($this->_s, $charlist, $lower);
    }

    /**
     * Truncates string to maximal length.
     *
     * @param int $maxLen
     * @param string|UnicodeString $append UTF-8 encoding
     * @return self
     */
    public function truncate(int $maxLen, string $append = "\xE2\x80\xA6"): self
    {
        $this->_s = Strings::truncate($this->_s, $maxLen, $append);
        return $this;
    }

    /**
     * @param string|UnicodeString $prepend
     * @return self
     */
    public function prepend($prepend): self
    {
        $this->_s = strval($prepend).$this->_s;
        return $this;
    }

    /**
     * @param string|UnicodeString $append
     * @return self
     */
    public function concat($append): self
    {
        $this->_s .= strval($append);
        return $this;
    }

    /**
     * Indents the content from the left.
     *
     * @param int $level
     * @param string $chars
     * @return self
     * @throws RegexpException
     */
    public function indent($level = 1, $chars = "\t"): self
    {
        if ($level > 0) {
            $this->replace(
                '#(?:^|[\r\n]+)(?=[^\r\n])#'
              , '$0' . str_repeat($chars, $level)
            );
        }
        return $this;
    }

    /**
     * Convert to lower case.
     *
     * @return self
     */
    public function lower(): self
    {
        $this->_s = Strings::lower($this->_s);
        return $this;
    }

    /**
     * Convert to upper case.
     *
     * @return self
     */
    public function upper(): self
    {
        $this->_s = Strings::upper($this->_s);
        return $this;
    }

    /**
     * @return self
     */
    public function firstUpper(): self
    {
        $this->_s = $this->getValue($this->substring(0, 1))
            ->upper()
            ->concat($this->substring(1))
        ;
        return $this;
    }

    /**
     * @return self
     */
    public function capitalize(): self
    {
        $this->_s = Strings::capitalize($this->_s);
        return $this;
    }

    /**
     * Returns UTF-8 string length.
     *
     * @return int
     */
    public function length(): int
    {
        return Strings::length($this->_s);
    }

    /**
     * @param string $charlist
     * @param int $sides
     * @return self
     * @throws RegexpException
     */
    public function trim($charlist = Strings::TRIM_CHARACTERS, $sides = self::TRIM_BOTH): self
    {
        $charlist = preg_quote($charlist, '#');
        $rePart = '';
        if ($sides <= 0) {
            $rePart .= '^['.$charlist.']+';
        }
        if ($sides === self::TRIM_BOTH) {
            $rePart .= '|';
        }
        if ($sides >= 0) {
            $rePart .= '['.$charlist.']+$';
        }
        return $this->replace('#'.$rePart.'#u', '');
    }

    /**
     * Pad a string to a certain length with another string.
     *
     * @param int $length
     * @param string $pad
     * @return self
     */
    public function padLeft(int $length, string $pad = ' '): self
    {
        $this->_s = Strings::padLeft($this->_s, $length, $pad);
        return $this;
    }

    /**
     * Pad a string to a certain length with another string.
     *
     * @param int $length
     * @param string $pad
     * @return self
     */
    public function padRight(int $length, string $pad = ' '): self
    {
        $this->_s = Strings::padRight($this->_s, $length, $pad);
        return $this;
    }

    /**
     * Splits string by a regular expression.
     *
     * @param string $pattern
     * @param int $flags
     * @return array
     * @throws RegexpException
     */
    public function split(string $pattern, int $flags = 0)
    {
        try {
            return Strings::split($this->_s, $pattern, $flags);
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (\Nette\Utils\RegexpException $e) {
            throw new RegexpException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Performs a regular expression match.
     *
     * @param string $pattern
     * @param int $flags can be PREG_OFFSET_CAPTURE (returned in bytes)
     * @param int $offset offset in bytes
     * @throws RegexpException
     * @return mixed
     */
    public function match(string $pattern, int $flags = 0, int $offset = 0)
    {
        try {
            return Strings::match($this->_s, $pattern, $flags, $offset);
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (\Nette\Utils\RegexpException $e) {
            throw new RegexpException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Performs a global regular expression match.
     *
     * @param string $pattern
     * @param int $flags can be PREG_OFFSET_CAPTURE (returned in bytes); PREG_SET_ORDER is default
     * @param int $offset offset in bytes
     * @throws RegexpException
     * @return array
     */
    public function matchAll(string $pattern, int $flags = 0, int $offset = 0)
    {
        try {
            return Strings::matchAll($this->_s, $pattern, $flags, $offset);
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (\Nette\Utils\RegexpException $e) {
            throw new RegexpException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param string $pattern
     * @param callable|string $replacement
     * @param int $limit
     * @return self
     * @throws RegexpException
     */
    public function replace($pattern, $replacement = NULL, $limit = -1): self
    {
        try {
            $this->_s = Strings::replace($this->_s, $pattern, $replacement, $limit);
        }
        /** @noinspection PhpRedundantCatchClauseInspection */
        catch (\Nette\Utils\RegexpException $e) {
            throw new RegexpException($e->getMessage(), $e->getCode(), $e);
        }
        catch (InvalidStateException $e) {
            throw new \InvalidArgumentException($e->getMessage());
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isBOM(): bool
    {
        return strval($this->substring(0, 3)) === "\xEF\xBB\xBF";
    }

    /**
     * @return self
     */
    public function removeBOM(): self
    {
        if ($this->isBOM()) {
            return $this->substring(3);
        }
        return $this;
    }

    /**
     * @return self
     */
    public function crlf2lf(): self
    {
        $this->_s = str_replace("\r\n", "\n", $this->_s);
        return $this;
    }

    /**
     * @return self
     */
    public function toCamelCase(): self
    {
        $this->_s = lcfirst(preg_replace('/[^a-z]/i', '', ucwords($this->_s, '_-')));
        return $this;
    }

    /**
     * @return self
     * @throws RegexpException
     */
    public function toSnakeCase(): self
    {
        $this->_s[0] = strtolower($this->_s[0]);
        $this->replace('/([A-Z])/', function($c)
        {
            return "_" . strtolower($c[1]);
        });
        return $this;
    }
}
