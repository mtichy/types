<?php

namespace MTi\Type;

use DivisionByZeroError;
use JsonSerializable;
use LogicException;
use MTi\InvalidNumberException;


class ExtendedFloat
    implements JsonSerializable
{
    /**
     * @param $value
     * @param int $decimals
     * @return ExtendedFloat
     * @throws InvalidNumberException
     */
    public static function floorToPrecision($value, int $decimals): self
    {
        $float = new ExtendedFloat($value);
        $shifter = pow(10, $decimals);
        return new ExtendedFloat(floor($shifter * $float->float()) / $shifter);
    }

    /**
     * @param float $f
     * @return ExtendedFloat
     */
    public static function fromFloat(float $f): self
    {
        try {
            return new self($f);
        }
        catch (InvalidNumberException $e) {
            throw new LogicException();
        }
    }

    /**
     * ExtendedFloat constructor.
     *
     * @param mixed $value
     * @param int $decimalsNumber Maximal count of decimals
     * @throws InvalidNumberException
     */
    public function __construct($value = 0, int $decimalsNumber = 6)
    {
        if (
            (!(is_numeric($value)) && !($value instanceof self))
            || (
                is_string($value)
                && ($value[0] == '.' || $value[strlen($value) - 1] == '.')
            )
        ) {
            throw new InvalidNumberException($value);
        }
        $this->setDecimalsNumber($decimalsNumber);
        $this->add($this->strip($value));
    }
    private $_float = 0.0;
    private $_decNumber;

    protected function strip($value)
    {
        return is_string($value)
            ? str_replace(
                  [',', ' ']
                , ['.', '']
                , $value
              )
            : $value
        ;
    }

    /**
     * Reset to zero.
     *
     * @return self
     */
    public function reset(): self
    {
        $this->setFloat(0);
        return $this;
    }

    /**
     * Set maximal count of decimals.
     *
     * @param int $decimalsNumber
     * @return self
     */
    public function setDecimalsNumber(int $decimalsNumber): self
    {
        $this->_decNumber = $decimalsNumber;
        $this->setFloat($this->float());
        return $this;
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return self
     */
    public function add($value): self
    {
        $this->setFloat($this->float() + $this->getValue($value));
        return $this;
    }

    private function getValue($value): float
    {
        if ($value instanceof self) {
            return $value->float();
        }
        else {
            return floatval($value);
        }
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return self
     */
    public function sub($value): self
    {
        $this->setFloat($this->float() - $this->getValue($value));
        return $this;
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return self
     */
    public function mul($value): self
    {
        $this->setFloat($this->float() * $this->getValue($value));
        return $this;
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return self
     */
    public function div($value): self
    {
        if (!$this->getValue($value)) {
            throw new DivisionByZeroError();
        }
        $this->setFloat($this->float() / $this->getValue($value));
        return $this;
    }

    /**
     * @param ExtendedFloat $ef
     * @return self
     */
    public function max(ExtendedFloat $ef): self
    {
        $this->setFloat(max(array_map(function (ExtendedFloat $v)
        {
            return $v->float();
        }, [$this, $ef])));
        return $this;
    }

    /**
     * @param ExtendedFloat $ef
     * @return self
     */
    public function min(ExtendedFloat $ef): self
    {
        $this->setFloat(min([$this->float(), $ef->float()]));
        return $this;
    }

    /**
     * @return self
     */
    public function abs(): self
    {
        $this->setFloat(abs($this->float()));
        return $this;
    }

    /**
     * @param ExtendedFloat $float
     * @return int -1/0/1
     */
    public function compare(ExtendedFloat $float): int
    {
        if ($this->isGreaterThan($float)) {
            return 1;
        }
        if ($this->isLessThan($float)) {
            return -1;
        }
        return 0;
    }

    protected function setFloat(float $f)
    {
        $this->_float = round($f, $this->decimalsNumber());
    }

    /**
     * Get float representation.
     *
     * @return float
     */
    public function float(): float
    {
        return $this->_float;
    }

    /**
     * Get maximal count of decimals.
     *
     * @return int
     */
    public function decimalsNumber(): int
    {
        return $this->_decNumber;
    }

    /**
     * Get value after decimal point.
     *
     * @return int
     */
    public function decimals(): int
    {
        return intval(round(
            ($this->float() - $this->integer())
                * pow(10, $this->decimalsNumber())
        ));
    }

    /**
     * Get integer representation.
     *
     * @return int
     */
    public function integer(): int
    {
        return intval($this->float());
    }

    /**
     * @param int $decimals
     * @return self
     */
    public function round(int $decimals = 0): self
    {
        $this->setFloat(round($this->float(), $decimals));
        return $this;
    }

    /**
     * @param int $decimals
     * @return self
     */
    public function ceil(int $decimals = 0): self
    {
        if ($decimals == 0) {
            $this->setFloat(ceil($this->float()));
        }
        else {
            $origDecNumber = $this->decimalsNumber();
            $shifter = pow(10, $decimals);
            $this
                ->setDecimalsNumber($origDecNumber + $decimals)
                ->mul($shifter)->ceil()->div($shifter)
                ->setDecimalsNumber($origDecNumber)
            ;
        }
        return $this;
    }

    /**
     * @param int $decimals
     * @return ExtendedFloat
     */
    public function floor(int $decimals = 0): self
    {
        if ($decimals === 0) {
            $this->setFloat(floor($this->float()));
        }
        else {
            $origDecNumber = $this->decimalsNumber();
            $shifter = pow(10, $decimals);
            $this
                ->setDecimalsNumber($origDecNumber + $decimals)
                ->mul($shifter)->floor()->div($shifter)
                ->setDecimalsNumber($origDecNumber)
            ;
        }
        return $this;
    }

    /**
     * Force negative value.
     *
     * @return self
     */
    public function negative(): self
    {
        $fl = clone $this;
        $fl->setFloat(-$fl->float());
        return $fl;
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @param float $tolerance
     * @return bool
     */
    public function equals($value, float $tolerance = 0.000001): bool
    {
        $f = $this->getValue($value);
        return $this->float() > $f - $tolerance
            && $this->float() < $f + $tolerance
        ;
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return bool
     */
    public function isLessThan($value): bool
    {
        return $this->float() < $this->getValue($value);
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return bool
     */
    public function isGreaterThan($value): bool
    {
        return $this->float() > $this->getValue($value);
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return bool
     */
    public function isAtMost($value): bool
    {
        return $this->isLessThan($value) || $this->equals($value);
    }

    /**
     * @param ExtendedFloat|mixed $value
     * @return bool
     */
    public function isAtLeast($value): bool
    {
        return $this->isGreaterThan($value) || $this->equals($value);
    }

    /**
     * @param ExtendedFloat|mixed $valueMin
     * @param ExtendedFloat|mixed $valueMax
     * @return bool
     */
    public function isBetween($valueMin, $valueMax): bool
    {
        return $this->isAtLeast($valueMin) && $this->isAtMost($valueMax);
    }

    /**
     * @return self
     */
    public function cl(): self
    {
        return clone $this;
    }

    public function jsonSerialize()
    {
        return $this->float();
    }

    public function __toString()
    {
        return sprintf('%.0'.$this->decimalsNumber().'f', $this->_float);
    }
}
