<?php

namespace MTi\Type;


abstract class Money
    extends ExtendedFloat
{
    public function __construct($value = 0, int $decimalsNumber = 2, string $ds = ',', string $ts = ' ')
    {
        parent::__construct($value, $decimalsNumber);
        $this->decimalsSeparator = $ds;
        $this->thousandsSeparator = $ts;
    }
    private $thousandsSeparator;
    private $decimalsSeparator;

    protected function format(int $d, string $ds, string $ts): string
    {
        return number_format($this->float(), $d, $ds, $ts);
    }

    abstract protected function withCurrency(string $v): string;

    protected function withoutSpaces($v): string
    {
        return str_replace(' ', '', $v);
    }

    /**
     * @param string $separator
     * @return Money
     */
    public function setThousandsSeparator(string $separator): Money
    {
        $this->thousandsSeparator = $separator;
        return $this;
    }

    /**
     * @param int $decimals
     * @return string
     */
    public function formattedNumber(int $decimals = 2): string
    {
        return $this->format(
            $decimals
          , $this->decimalsSeparator
          , $this->thousandsSeparator
        );
    }

    /**
     * @param int $decimals
     * @return string
     */
    public function formattedNumberWithDots(int $decimals = 2): string
    {
        return $this->format(
            $decimals
          , ','
          , '.'
        );
    }

    /**
     * @param int $decimals
     * @return string
     */
    public function formattedNumberWithOutSpaces(int $decimals = 2): string
    {
        return $this->format(
            $decimals
          , ','
          , ''
        );
    }

    /**
     * @param int $decimals
     * @return string
     */
    public function roundedFormattedNumber(int $decimals = 0): string
    {
        /** @var Money $m */
        $m = $this->round();
        return $m->format(
            $decimals
          , $this->decimalsSeparator
          , $this->thousandsSeparator
        );
    }

    /**
     * @param int $decimals
     * @return string
     */
    public function roundedNumberWithCurrency(int $decimals = 0): string
    {
        return $this->withCurrency(
            $this->roundedFormattedNumber($decimals)
        );
    }

    /**
     * @return string
     */
    public function formattedNumberWithCurrency(): string
    {
        return $this->withCurrency(
            $this->formattedNumber()
        );
    }

    /**
     * @return string
     */
    public function formattedNumberWithCurrencyAndHyphen(): string
    {
        if ($this->decimals()) {
            return $this->formattedNumberWithCurrency();
        }
        return $this->withCurrency(
            sprintf('%s,-', $this->roundedFormattedNumber())
        );
    }

    /**
     * @return string
     */
    public function compactNumberWithDotsAndCurrency(): string
    {
        /** @var $cl Money */
        $cl = $this->cl();
        $render = $cl->setThousandsSeparator('.');
        return $render->withoutSpaces(
            $render->formattedNumberWithCurrencyAndHyphen()
        );
    }

    /**
     * @param int $decimals
     * @return string
     */
    public function formattedNumberWithoutZeroSuffix(int $decimals = 2): string
    {
        return $this->format(
            $this->decimals() ? $decimals : 0
          , $this->decimalsSeparator
          , $this->thousandsSeparator
        );
    }

    /**
     * @return string
     */
    public function floatNumberWithZeroSuffix(): string
    {
        return $this->format(
            $this->decimalsNumber()
          , '.'
          , ''
        );
    }
}
