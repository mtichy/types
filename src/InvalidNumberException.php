<?php

namespace MTi;


class InvalidNumberException
    extends \RuntimeException
{
    public function __construct($inputValue)
    {
        parent::__construct(sprintf(
            "Invalid numeric value '%s'."
          , strval($inputValue)
        ));
    }
}
