<?php

namespace MTi\Util;


final class Arrays
{
    /**
     * Returns array item or $default if item is not set.
     *
     * @param array $arr
     * @param string|int $key
     * @param null $default
     * @return array|mixed|null
     */
    public static function get(array $arr, $key, $default = NULL)
    {
        foreach (is_array($key) ? $key : [$key] as $k) {
            if (is_array($arr) && array_key_exists($k, $arr)) {
                $arr = $arr[$k];
            }
            else {
                if (func_num_args() < 3) {
                    throw new \InvalidArgumentException("Missing item '$k'.");
                }
                return $default;
            }
        }
        return $arr;
    }

    /**
     * Returns reference to array item.
     *
     * @param array|null $arr
     * @param mixed $key
     * @return mixed
     */
    public static function &getRef(&$arr, $key)
    {
        foreach (is_array($key) ? $key : array($key) as $k) {
            if (is_array($arr) || $arr === NULL) {
                $arr = & $arr[$k];
            }
            else {
                throw new \InvalidArgumentException('Traversed item is not an array.');
            }
        }
        return $arr;
    }

    /**
     * Amends $arr1 tree (multi-dimensional array) with array tree $arr2.
     *
     * @param array $arr1
     * @param array $arr2
     * @return array
     */
    public static function mergeTree(array $arr1, array $arr2): array
    {
        $res = $arr1 + $arr2;
        foreach (array_intersect_key($arr1, $arr2) as $k => $v) {
            if (is_array($v) && is_array($arr2[$k])) {
                $res[$k] = self::mergeTree($v, $arr2[$k]);
            }
        }
        return $res;
    }

    /**
     * Searches the array for a given key and returns the offset if successful.
     *
     * @param array $arr
     * @param string|int $key
     * @return int offset if it is found, FALSE otherwise
     */
    public static function searchKey(array $arr, $key)
    {
        $foo = [$key => NULL];
        return array_search(key($foo), array_keys($arr), TRUE);
    }

    /**
     * @param array $arr
     * @param string|int $key
     * @param array $inserted
     */
    public static function insertBefore(array &$arr, $key, array $inserted)
    {
        $offset = self::searchKey($arr, $key);
        $arr = array_slice($arr, 0, $offset, TRUE)
            + $inserted
            + array_slice($arr, $offset, count($arr), TRUE)
        ;
    }

    /**
     * @param array $arr
     * @param string|int $key
     * @param array $inserted
     */
    public static function insertAfter(array &$arr, $key, array $inserted)
    {
        $offset = self::searchKey($arr, $key);
        $offset = $offset === FALSE ? count($arr) : $offset + 1;
        $arr = array_slice($arr, 0, $offset, TRUE)
            + $inserted
            + array_slice($arr, $offset, count($arr), TRUE)
        ;
    }

    /**
     * @param array $arr
     * @param string|int $oldKey
     * @param string|int $newKey
     */
    public static function renameKey(array &$arr, $oldKey, $newKey)
    {
        $offset = self::searchKey($arr, $oldKey);
        if ($offset !== FALSE) {
            $keys = array_keys($arr);
            $keys[$offset] = $newKey;
            $arr = array_combine($keys, $arr);
        }
    }

    /**
     * Ensures required array "path" through multi-dimensional array tree.
     *
     * @param array $array
     * @param array $path List of required keys, from root below
     */
    public static function makePath(array &$array, array $path)
    {
        $key = array_shift($path);
        if (is_null($key)) {
            return;
        }
        if (!array_key_exists($key, $array)) {
            $array[$key] = [];
        }
        self::makePath($array[$key], $path);
    }
}
