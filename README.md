# Extended PHP types library

This library provides extended data types and object alternatives for existing PHP types.

## Installation

via composer: 
```sh
composer require mtichy/types
```
or you can just download project source files directly

## Usage

with composer: 
```php
<?php
require __DIR__.'/vendor/autoload.php';
```

without composer 
```php
<?php
require 'path-to-TYPE-dir/autoload.php';
```
